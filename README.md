# A set of SQL functions for performing geospatial data conflation, as described in the following article:

Lei, T.L. (2020, in press). Geospatial data conflation: a formal approach based on optimization and relational databases. International Journal of Geographical Information Science.

Copyright (c) 2020 Ting Lei (leiting@gmail.com).


## Detail

    This repository contains the SQL functions that define the conflation functions and interface with the underlying PostgreSQL extensions for 1) solving mincost network flow problem (pgNetworkflow) and 2) computing certain distances between geometries (pgGEOS).

## Directories

  sql/         all code in SQL files.


## Install instructions

The functions in this repository are simple SQL functions that can be directly executed using a PostgreSQL client such as psql. They require the standard PostGIS extension, as well as the above-mentioned two extensions (pgnetworkflow and pggeos). Once the dependencies are installed, the SQL functions can be installed e.g., by executing the following shell script:

    ./update_cfl_sql.sh
    

## License

These functions are released under the [GPL v2 License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html), same as PostGIS.

