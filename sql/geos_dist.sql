--**********************************************************************
-- 
-- Exposing GEOS interface functions for use by PostGIS
--
-- Copyright (C) 2020  Ting Lei <leiting@gmail.com>
--
-- This is free software; you can redistribute and/or modify it under
-- the terms of the GNU General Public Licence as published
-- by the Free Software Foundation.
-- See the COPYING file for more information. *

--**********************************************************************

-------------
-- TOC: types
DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typnamespace::regnamespace::text = 'public' AND typname = 'geometry_name') THEN
    CREATE TYPE geometry_name AS (geom geometry, name text);
  END IF;
END$$;

CREATE OR REPLACE FUNCTION cast_geometry_name_to_geometry(gn geometry_name)
  RETURNS geometry AS $func$
  SELECT (gn).geom as geom;
$func$  LANGUAGE sql;

DROP CAST IF EXISTS (geometry_name AS geometry);
CREATE CAST (geometry_name AS geometry) WITH FUNCTION cast_geometry_name_to_geometry(geometry_name) AS IMPLICIT;


---------------------------------


--wrapper
CREATE OR REPLACE FUNCTION ST_DirectedHausdorffDistance(geom1 geometry, geom2 geometry)
RETURNS FLOAT8 AS $func$
  SELECT * FROM ST_DirectedHausdorffDistance_geos(ST_AsEWKB(geom1), ST_AsEWKB(geom2));
$func$ LANGUAGE 'sql';


--wrapper
CREATE OR REPLACE FUNCTION ST_DirectedHausdorffDistanceDensify(geom1 geometry, geom2 geometry, densifyFrac float8)
RETURNS FLOAT8 AS $func$
  SELECT * FROM ST_DirectedHausdorffDistanceDensify_geos(ST_AsEWKB(geom1), ST_AsEWKB(geom2), densifyFrac);
$func$ LANGUAGE 'sql';


CREATE OR REPLACE FUNCTION ST_Measure(g geometry)
  RETURNS FLOAT8 AS $func$
  SELECT CASE 
    WHEN ST_GeometryType(g) in ('ST_Point','ST_MultiPoint')           THEN 0
    WHEN ST_GeometryType(g) in ('ST_LineString','ST_MultiLineString') THEN ST_Length(g)
    WHEN ST_IsSolid(g) THEN ST_Volume(g)
    ELSE ST_Area(g)
    END;
$func$  LANGUAGE sql;

--e.g. SELECT ST_Measure(ST_GeomFromText('LINESTRING(77.29 29.07,77.42 29.26,77.27 29.31,77.29 29.07)'));

